#!/usr/bin/env bash
#
# Deploys a Helm chart to a Kubernetes cluster running on Azure Kubernetes Services (AKS)
#
# Required globals:
#   NAME
#
# Optional globals:
#   DEBUG (default: "false")

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
  fi
}
enable_debug

# azure service principal
# these are required for at least one execution of the pipe in a step in order to fetch the kube context from the aks cluster
AZURE_APP_ID=${AZURE_APP_ID}
AZURE_PASSWORD=${AZURE_PASSWORD}
AZURE_TENANT_ID=${AZURE_TENANT_ID}

# azure kubernetes cluster AKS
# always required
AZURE_AKS_NAME=${AZURE_AKS_NAME:?'AZURE_AKS_NAME environment variable missing.'}
AZURE_RESOURCE_GROUP=${AZURE_RESOURCE_GROUP:?'AZURE_RESOURCE_GROUP environment variable missing.'}

# default parameters
DEBUG=${DEBUG:="false"}

# Helm parameters
HELM_VERSION=${HELM_VERSION:="latest"}
HELM_COMMAND=${HELM_COMMAND}
HELM_COMMAND_ARGS=${HELM_COMMAND_ARGS}
HELM_COMMAND_WAIT=${HELM_COMMAND_WAIT:="true"}
HELM_RELEASE_NAME=${HELM_RELEASE_NAME}
HELM_CHART_DIR=${HELM_CHART_DIR}
HELM_CHART_NAME=${HELM_CHART_NAME}
HELM_UPGRADE_INSTALL_IF_NOT_PRESENT=${HELM_INSTALL_IF_NOT_PRESENT:="true"}

# look for existing kubeconfig file and re-use, otherwise authenticate to azure and generate one
# a kubeconfig file will exist if this pipe has already run, for the same AKS cluster within this step
if [ ! -f .kube/kubeconfig-"${AZURE_RESOURCE_GROUP}-${AZURE_AKS_NAME}" ]; then

  info "no existing kube config found at .kube/kubeconfig-${AZURE_RESOURCE_GROUP}-${AZURE_AKS_NAME}, retrieving from Azure"

  # check for azure service principal environment variables
  if [[ -z "${AZURE_APP_ID}" ]] || [[ -z "${AZURE_PASSWORD}" ]] || [[ -z "${AZURE_TENANT_ID}" ]]; then
    fail "AZURE_APP_ID, AZURE_PASSWORD, AZURE_TENANT_ID are missing, cannot authenticate to Azure"    
  fi

  # log in to the azure cli
  info "log in the azure cli using service principal"
  run az login --service-principal --username "${AZURE_APP_ID}" --password "${AZURE_PASSWORD}" --tenant "${AZURE_TENANT_ID}"
  if [[ "${status}" != "0" ]]; then  
    fail "Error logging in using azure service principal!"
  fi

  # retrieve the kubernetes config for kube context
  info "retrieve the kube config via the azure cli"
  run az aks get-credentials  --resource-group "${AZURE_RESOURCE_GROUP}" --name "${AZURE_AKS_NAME}" --file .kube/kubeconfig-"${AZURE_RESOURCE_GROUP}-${AZURE_AKS_NAME}" --overwrite-existing
  if [[ "${status}" != "0" ]]; then
    fail "Unable to retrieve the kubernetes config file from the cluster using az aks get credentials!"
  fi
else
  info "existing kube config detected at .kube/kubeconfig-${AZURE_RESOURCE_GROUP}-${AZURE_AKS_NAME}"
fi

# set the kube context to point to our file
info "setting the kube config current context"
export KUBECONFIG=.kube/kubeconfig-"${AZURE_RESOURCE_GROUP}-${AZURE_AKS_NAME}"
run kubectl config use-context "${AZURE_AKS_NAME}"

echo helm version: "${HELM_VERSION}"
echo helm command: "${HELM_COMMAND}"

# install helm tools
info "installing HELM client version=${HELM_VERSION}"
[ -d ./tool-install-temp ] || mkdir ./tool-install-temp
[ -d ./tool-install-temp/helm ] || mkdir ./tool-install-temp/helm

HELM_3="false"
if [[ $HELM_VERSION == "latest" ]] || [[ -z ${HELM_VERSION} ]]; then
  HELM_VERSION=""
  HELM_3="true"
elif [[ ${HELM_VERSION%.*.*} == "v3" ]]; then
  HELM_3="true"
fi

if [[ $HELM_3 == "true" ]]; then
  GET_HELM_SCRIPT="get-helm-3"
else
  GET_HELM_SCRIPT="get"
fi

run curl -fsSL "https://raw.githubusercontent.com/helm/helm/master/scripts/${GET_HELM_SCRIPT}" -o ./tool-install-temp/helm/get_helm.sh
run chmod 700 ./tool-install-temp/helm/get_helm.sh
if [[ -z ${HELM_VERSION} ]]; then
  run ./tool-install-temp/helm/get_helm.sh
else
  run ./tool-install-temp/helm/get_helm.sh  --version ${HELM_VERSION}
fi

if [[ $HELM_3 == "true" ]]; then
  # check helm version
  run helm version
else
  # helm init client
  run helm init --client-only

  # check helm client version
  run helm version -c
fi

# chart directory or chart name
HELM_CHART=""
if [[ -n $HELM_CHART_DIR ]]; then
  info "using helm chart directory ${HELM_CHART_DIR}"
  HELM_CHART="${HELM_CHART_DIR}"
else
  if [[ $HELM_3 == "true" ]]; then
    if [[ -z $HELM_CHART_REPO_URL ]] || [[ -z $HELM_CHART_REPO_NAME ]]; then
      HELM_CHART_REPO_NAME=stable
      HELM_CHART_REPO_URL=https://kubernetes-charts.storage.googleapis.com/
    fi

    info "adding helm repo ${HELM_CHART_REPO_NAME} from ${HELM_CHART_REPO_URL}"
    run helm repo add ${HELM_CHART_REPO_NAME} ${HELM_CHART_REPO_URL}
  fi
  if [[ -n $HELM_CHART_NAME ]]; then
    info "using helm chart repo/name ${HELM_CHART_NAME}"
    HELM_CHART="${HELM_CHART_NAME}"
    # make sure we update list of repositories if not using local chart
    run helm repo update
  fi
fi

echo "HELM_CHART = ${HELM_CHART}"

# extra args
args=()
if [[ ${HELM_COMMAND_WAIT} ]]; then args+=( '--wait' ); fi

# helm command handler
if [[ -n ${HELM_COMMAND} ]]; then
  case $HELM_COMMAND in
      "install")
        info "running helm install"
        run helm install ${HELM_COMMAND_ARGS}
      ;;
      "upgrade")
        info "running helm upgrade"
        if [[ ${HELM_UPGRADE_INSTALL_IF_NOT_PRESENT} ]]; then args+=( '--install' ); fi
        if [ -z ${HELM_CHART} ] ||  [ -z ${HELM_RELEASE_NAME} ]; then
          fail "(HELM_CHART_DIR | HELM_CHART_NAME) & HELM_RELEASE_NAME are required for helm upgrade"
        fi
        run helm upgrade ${HELM_RELEASE_NAME} ${HELM_CHART} ${HELM_COMMAND_ARGS} "${args[@]}"
      ;;
      "delete")
        info "running helm delete"
        run helm install ${HELM_COMMAND_ARGS}
      ;;
      "")
      ;;
      *)
        info "running helm command ${HELM_COMMAND} using generic handler"
        run helm ${HELM_COMMAND} ${HELM_COMMAND_ARGS}
      ;;
  esac
fi


if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi
